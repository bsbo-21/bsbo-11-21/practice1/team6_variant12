using System;
using System.Collections.Generic;

namespace ConsoleApp3
{
    public enum TestStatus
    {
        PASSED,  // Пройдено
        FAILED,  // Не пройдено
        SKIPPED  // Пропущено
    }

    public class Error
    {
        public string Message { get; set; }  // Сообщение об ошибке
        public string StackTrace { get; set; }  // Стек вызовов при ошибке
    }

    public class TestResult
    {
        public TestStatus Status { get; set; }  // Статус результата теста
        public List<Error> Errors { get; set; }  // Список ошибок

        public TestResult()
        {
            Errors = new List<Error>();
        }
    }

    public class TestCase
    {
        public TestResult Execute()
        {
            // Выполнение логики тестового случая
            TestResult result = new TestResult();

            // Установка статуса и добавление ошибок при необходимости
            result.Status = TestStatus.PASSED;  // Успешно

            // Добавление ошибки для демонстрации
            Error error = new Error()
            {
                Message = "Пример сообщения об ошибке",
                StackTrace = "Пример стека вызовов"
            };
            result.Errors.Add(error);

            return result;
        }
    }

    public class TestScenario
    {
        public List<TestCase> TestCases { get; set; }  // Список тестовых случаев

        public TestScenario()
        {
            TestCases = new List<TestCase>();
        }

        public void AddTestCase(TestCase testCase)
        {
            TestCases.Add(testCase);
        }

        public void RemoveTestCase(TestCase testCase)
        {
            TestCases.Remove(testCase);
        }
    }

    public class TestSuite
    {
        public List<TestScenario> TestScenarios { get; set; }  // Список тестовых сценариев

        public TestSuite()
        {
            TestScenarios = new List<TestScenario>();
        }

        public void AddTestScenario(TestScenario testScenario)
        {
            TestScenarios.Add(testScenario);
        }

        public void RemoveTestScenario(TestScenario testScenario)
        {
            TestScenarios.Remove(testScenario);
        }

        public List<TestResult> RunTests()
        {
            List<TestResult> testResults = new List<TestResult>();

            foreach (TestScenario testScenario in TestScenarios)
            {
                foreach (TestCase testCase in testScenario.TestCases)
                {
                    TestResult result = testCase.Execute();
                    testResults.Add(result);
                }
            }

            return testResults;
        }
    }

    public class Developer
    {
        public TestCase CreateTestCase()
        {
            // Создание нового тестового случая
            TestCase testCase = new TestCase();

            // Добавление логики для установки параметров, шагов тестирования и ожидаемых результатов

            return testCase;
        }

        public void UpdateTestCase(TestCase testCase)
        {
            // Обновление существующего тестового случая
            // Добавление логики для обновления свойств тестового случая
        }

        public void DeleteTestCase(TestCase testCase)
        {
            // Удаление существующего тестового случая
            // Добавление логики для удаления тестового случая из хранилища или памяти
        }
    }

    public class QAEngineer
    {
        public void AnalyzeTestResults(List<TestResult> testResults)
        {
            // Анализ результатов тестирования
            // Добавление логики для анализа результатов тестирования и создания отчета
        }

        public TestScenario CreateTestScenario()
        {
            // Создание нового тестового сценария
            TestScenario testScenario = new TestScenario();

            // Добавление логики для установки деталей тестового сценария

            return testScenario;
        }

        public void UpdateTestScenario(TestScenario testScenario)
        {
            // Обновление существующего тестового сценария
            // Добавление логики для обновления свойств тестового сценария
        }

        public void DeleteTestScenario(TestScenario testScenario)
        {
            // Удаление существующего тестового сценария
            // Добавление логики для удаления тестового сценария из хранилища или памяти
        }
    }



    public class System
    {

        private TestSuite testSuite;
        private List<TestCase> testCases;



        public void RunningTests()
        {
            List<TestResult> testResults = testSuite.RunTests();
            // Логика для выполнения тестов и анализа результатов
        }


        public void CreateTestCase()
        {
            TestCase testCase = new TestCase();
            testCases.Add(testCase);
           
        }


        public void CreateTestScenario()
        {
            QAEngineer qaEngineer = new QAEngineer();
            TestScenario testScenario = qaEngineer.CreateTestScenario();
            testSuite.AddTestScenario(testScenario);
            
        }


    }



    public class Program
    {
        public static void Main(string[] args)
        {
            
            System system = new System();

            // Создание тестовых случаев
            system.CreateTestCase();

            // Создание тестовых сценариев
            system.CreateTestScenario();

            // Запуск тестов
            system.RunningTests();


        }
    }
}
